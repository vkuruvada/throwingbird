﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.Linq;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public CameraFollow cameraFollow;

    int currentBirdIndex;

    public SlingShot slingShot;

    public static GameState gameState;

    private List<GameObject> bricks;
    private List<GameObject> birds;
    private List<GameObject> pigs;

    private void Awake()
    {
        gameState = GameState.Start;
        slingShot.enabled = false;

        bricks = new List<GameObject>(GameObject.FindGameObjectsWithTag("Brck"));
        birds = new List<GameObject>(GameObject.FindGameObjectsWithTag("Bird"));
        pigs = new List<GameObject>(GameObject.FindGameObjectsWithTag("Pig"));


        slingShot.slingShotLineRenderer1.enabled = false;
        slingShot.slingShotLineRenderer2.enabled = false;
    }

    private void OnEnable()
    {
        slingShot.birdThrown += SlingShotThrow;
    }

    private void OnDisable()
    {
        slingShot.birdThrown -= SlingShotThrow;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
        switch(gameState)
        {
            case GameState.Start:

                if (Input.GetMouseButtonUp(0))
                {
                    AnimateBirdToSlingShot();
                }
                break;

            case GameState.Playing:
                if (slingShot.slingShotState == SlingShotState.BirdFlying &&
                    (BricksBirdsPigsStoppedMoving() || Time.time - slingShot.timeSinceThrow > 5f))
                {
                    slingShot.enabled = false;
                    AnimateCameraToStartPosition();
                    gameState = GameState.BirdMovingToSlingShot;
                }

                slingShot.slingShotLineRenderer1.enabled = false;
                slingShot.slingShotLineRenderer2.enabled = false;

                break;

            case GameState.Won:
            case GameState.Lost:
                if (Input.GetMouseButtonDown(0))
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                }
                break;
        }
	}

    void AnimateBirdToSlingShot()
    {
        gameState = GameState.BirdMovingToSlingShot;
        birds[currentBirdIndex].transform.positionTo(
                Vector2.Distance(
                    birds[currentBirdIndex].transform.position / 10,
                    slingShot.birdWaitPosition.position) / 10,
                    slingShot.birdWaitPosition.position
            ).setOnCompleteHandler((x) =>
            {
                x.complete();
                x.destroy();

                gameState = GameState.Playing;
                slingShot.enabled = true;

                slingShot.birdToThrow = birds[currentBirdIndex];
            });
        slingShot.slingShotLineRenderer1.enabled = true;
        slingShot.slingShotLineRenderer2.enabled = true;
    }

    bool BricksBirdsPigsStoppedMoving()
    {
        foreach(var item in bricks.Union(birds).Union(pigs))
        {
            if (item != null && item.GetComponent<Rigidbody2D>().velocity.sqrMagnitude > GameVariables.MinVelocity)
            {
                return false;
            }
        }

        return true;
    }

    private bool AllPigsDestroyed()
    {
        return pigs.All(x => x == null);
    }

    private void AnimateCameraToStartPosition()
    {
        float duration = Vector2.Distance(Camera.main.transform.position, cameraFollow.startingPosition) / 10;
        if (duration == 0.0f)
        {
            duration = 0.1f;
        }

        Camera.main.transform.positionTo(duration, cameraFollow.startingPosition)
            .setOnCompleteHandler((x) =>
            {
                cameraFollow.isFollowing = false;
                if(AllPigsDestroyed())
                {
                    gameState = GameState.Won;
                } else if (currentBirdIndex == birds.Count -1)
                {
                    gameState = GameState.Lost;
                } else
                {
                    slingShot.slingShotState = SlingShotState.Idle;
                    currentBirdIndex++;
                    AnimateBirdToSlingShot();
                }
            });
    }

    private void SlingShotThrow()
    {
        cameraFollow.birdToFollow = birds[currentBirdIndex].transform;
        cameraFollow.isFollowing = true;
    }
}
