﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlingShot : MonoBehaviour {

    private Vector3 slingShotMiddleVector;

    public SlingShotState slingShotState;

    public Transform leftSlingShotOrigin, rightSlingShortOrigin;

    public LineRenderer slingShotLineRenderer1, slingShotLineRenderer2, trajectoryLineRenderer;

    public GameObject birdToThrow;

    public Transform birdWaitPosition;

    public float throwSpeed;

    public float timeSinceThrow;

    public delegate void BirdThrown();

    public event BirdThrown birdThrown;

    private void Awake()
    {
        slingShotLineRenderer1.sortingLayerName = "Foreground";
        slingShotLineRenderer2.sortingLayerName = "Foreground";
        trajectoryLineRenderer.sortingLayerName = "Foreground";

        slingShotState = SlingShotState.Idle;
        slingShotLineRenderer1.SetPosition(0, leftSlingShotOrigin.position);
        slingShotLineRenderer2.SetPosition(0, rightSlingShortOrigin.position);
        slingShotMiddleVector = new Vector3((leftSlingShotOrigin.position.x + rightSlingShortOrigin.position.x) / 2,
                                            (leftSlingShotOrigin.position.y + rightSlingShortOrigin.position.y) / 2, 0);
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

        switch(slingShotState)
        {
            case SlingShotState.Idle:
                InitializeBird();
                DislaySlingShotLineRenderers();
                if (Input.GetMouseButtonDown(0))
                {
                    Vector3 location = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                    if (birdToThrow.GetComponent<CircleCollider2D>() == Physics2D.OverlapPoint(location))
                    {
                        slingShotState = SlingShotState.UserPulling;
                    }
                }

                break;

            case SlingShotState.UserPulling:

                DislaySlingShotLineRenderers();

                if (Input.GetMouseButton(0))
                {
                    Vector3 location = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    location.z = 0f;

                    if (Vector3.Distance(location, slingShotMiddleVector) > 1.5f)
                    {
                        var maxPosition = (location - slingShotMiddleVector).normalized * 1.5f + slingShotMiddleVector;
                        birdToThrow.transform.position = maxPosition;
                    } else
                    {
                        birdToThrow.transform.position = location;
                    }

                    var distance = Vector3.Distance(slingShotMiddleVector, birdToThrow.transform.position);
                    DisplayTrajectoryLineRenderer(distance);
                } else
                {
                    SetTrajectoryLineRendererActive(true);
                    timeSinceThrow = Time.time;

                    float distance = Vector3.Distance(slingShotMiddleVector, birdToThrow.transform.position);
                    if (distance > 1)
                    {
                        SetSlingShotLineRendererActive(false);
                        slingShotState = SlingShotState.BirdFlying;
                        ThrowBird(distance);
                    } else
                    {
                        birdToThrow.transform.positionTo(distance / 10, birdWaitPosition.position);
                        InitializeBird();
                    }
                }

                break;

        }

    }

    public void InitializeBird() {
        birdToThrow.transform.position = birdWaitPosition.position;
        slingShotState = SlingShotState.Idle;
        SetSlingShotLineRendererActive(true);
    }

    public void SetSlingShotLineRendererActive(bool active)
    {
        slingShotLineRenderer1.enabled = active;
        slingShotLineRenderer2.enabled = active;
    }

    void DislaySlingShotLineRenderers()
    {
        slingShotLineRenderer1.SetPosition(1, birdToThrow.transform.position);
        slingShotLineRenderer2.SetPosition(1, birdToThrow.transform.position);
    }

    void SetTrajectoryLineRendererActive(bool active)
    {
        trajectoryLineRenderer.enabled = active;
    }

    void DisplayTrajectoryLineRenderer(float distance)
    {
        SetTrajectoryLineRendererActive(true);

        Vector3 v2 = slingShotMiddleVector - birdToThrow.transform.position;
        int segmentCount = 15;

        Vector2[] segments = new Vector2[segmentCount];

        Vector2 segVelocity = new Vector2(v2.x, v2.y) * throwSpeed * distance;
        segments[0] = birdToThrow.transform.position;

        for (int i=0; i< segments.Length; i++)
        {
            float time = Time.fixedDeltaTime * 5f;
            segments[i] = segments[0] + segVelocity * time + 0.5f * Physics2D.gravity * Mathf.Pow(time, 2);
        }

        trajectoryLineRenderer.positionCount = segmentCount;
        for (int i=0;i< segmentCount; i++)
        {
            trajectoryLineRenderer.SetPosition(i, segments[i]);
        }
    }

    private void ThrowBird(float distance)
    {
        Vector3 velocity = slingShotMiddleVector - birdToThrow.transform.position;
        birdToThrow.GetComponent<BirdScript>().OnThrow();
        birdToThrow.GetComponent<Rigidbody2D>().velocity = new Vector2(velocity.x, velocity.y) * throwSpeed * distance;

        if (birdThrown != null )
        {
            birdThrown();
        }
    }

}
