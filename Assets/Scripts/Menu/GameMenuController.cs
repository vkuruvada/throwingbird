﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameMenuController : MonoBehaviour {


    public void GoBack()
    {
        SceneManager.LoadScene("LevelMenu");
    }
}
