﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LevelMenuController : MonoBehaviour {


    public void PlayGame()
    {
        SceneManager.LoadScene("GamePlay");
    }

    public void GoBack()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
