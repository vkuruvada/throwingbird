﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour {

    private AudioSource audioSource;

    public float health = 70f;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Rigidbody2D body = collision.gameObject.GetComponent<Rigidbody2D>();
        if (body == null)
        {
            return;
        }

        float damage = body.velocity.magnitude * 10f;

        if (damage > 10f)
        {
            audioSource.Play();
        }

        health -= damage;

        if (health < 0)
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
