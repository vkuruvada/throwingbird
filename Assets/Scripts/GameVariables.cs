﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameVariables {

    public static float MinVelocity = 0.09f;
    public static float BirdColliderRadiusNormal = 0.5f;
    public static float BirdColliderRadiusBig = 0.6f;
}
